import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  public restuarentsdata: any[] = [

    {

      "resId": 0,

      "resName": "Al-Saj",

      "contactNumber": "9847456985",

      "latitude": "",

      "longitude": ""

    },

    {

      "resId": 1,

      "resName": "Tharavaadu",

      "contactNumber": "9454947659",

      "latitude": "",

      "longitude": ""

    },

    {

      "resId": 2,

      "resName": "Hotel Aryaas",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 3,

      "resName": "Hotel Sreedevi",

      "contactNumber": "95457656754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 4,

      "resName": "Malabar Cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 5,

      "resName": "May's Kitchen",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 6,

      "resName": "Lucamo art Cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 7,

      "resName": "De Cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 8,

      "resName": "Tibetan Cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 9,

      "resName": "Arabian Palace",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 10,

      "resName": "Al Reem",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 11,

      "resName": "Mosaic",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 12,

      "resName": "Malabar cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 13,

      "resName": "Teapot Cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 14,

      "resName": "Quissa Cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 15,

      "resName": "Loaner's Corner",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 16,

      "resName": "Thai Soul",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 17,

      "resName": "Zoka",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 18,

      "resName": "Sky Grill",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 19,

      "resName": "Momo's Cafe",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 20,

      "resName": "Cake Hut",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 21,

      "resName": "Rasoi Kochi",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {

      "resId": 22,

      "resName": " Cassava",

      "contactNumber": "95457646754",

      "latitude": "",

      "longitude": ""

    },
    {
      
      "resId": 23,
      
      "resName": "Farmers Cafe",
      
      "contactNumber":"95457646754",
      
      "latitude": "",
      
      "longitude": ""
      
      },
      {
      
        "resId": 24,
        
        "resName": "Lilliput Cafe",
        
        "contactNumber":"95457646754",
        
        "latitude": "",
        
        "longitude": ""
        
        },
        {
      
          "resId": 25,
          
          "resName": "Cilliout cafe",
          
          "contactNumber":"95457646754",
          
          "latitude": "",
          
          "longitude": ""
          
          },
          {
      
            "resId": 26,
            
            "resName": "Grand Hotel",
            
            "contactNumber":"95457646754",
            
            "latitude": "",
            
            "longitude": ""
            
            },
            {
      
              "resId": 27,
              
              "resName": "Grand Lounge",
              
              "contactNumber":"95457646754",
              
              "latitude": "",
              
              "longitude": ""
              
              },
              {
      
                "resId": 28,
                
                "resName": "Fusion Bay",
                
                "contactNumber":"95457646754",
                
                "latitude": "",
                
                "longitude": ""
                
                },
              



  ]


  public menudata: any[] = [
    {

      "menuTypeId": 1, "menuTypeName": "veg"

    },

    {

      "menuTypeId": 2, "menuTypeName": "non-veg"

    },

    {

      "menuTypeId": 3, "menuTypeName": "starters"

    },

    {

      "menuTypeId": 4, "menuTypeName": "desserts"

    },
    {

      "menuTypeId": 4, "menuTypeName": "drinks"

    }
  ]

  public resmenudata: any[] = [
    {

      "menuItemId": 1,

      "resId": 1,

      "menuTypeId": 1,

      "menuItemDesc": "gobi",

      "price": 100,

      "Image": ""

    },

    {

      "menuItemId ": 2,

      "resId": 1,

      "menuTypeId": 1,

      "menuItemsDesc": "paneer",

      "price": 120,

      "Image": ""

    },

    {

      "menuItemId ": 3,

      "resId": 1,

      "menuTypeId": 4,

      "menuItemDesc": "muffins-chocolate",

      "price": 50,

      "Image": ""

    },

    {

      "menuItemId ": 4,

      "resId": 1,

      "menuTypeId": 4,

      "menuItemDesc": "ice-creams",

      "price": 70,

      "Image": ""

    },

    {

      "menuItemId ": 5,

      "resId": 2,

      "menuTypeId": 4,

      "menuItemDesc": "ice-creams",

      "price": 75,

      "Image": ""

    }
  ]

  public orderentry: any[] = [
    {

      "orderId": 0,

      "resId": 1,

      "userPhone": "9555577777",

      "orderDetails": [

        {

          "menuItemId": 1,

          "amount": 100

        },

        {

          "menuItemId": 2,

          "amount": 120

        }

      ],

      "totalAmount": 220,

      "taxAmount": 50,

      "deliveryCharges": 50,

      "payableAmount": 320

    }

  ]


  constructor() { }
}
