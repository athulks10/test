import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DesertComponent } from 'src/restuarents/desert/desert.component';
import { DrinksComponent } from 'src/restuarents/drinks/drinks.component';
import { FormsComponent } from 'src/restuarents/forms/forms.component';
import { NonvegComponent } from 'src/restuarents/nonveg/nonveg.component';
import { StarterComponent } from 'src/restuarents/starter/starter.component';
import { VegComponent } from 'src/restuarents/veg/veg.component';
import { HomepageComponent } from './homepage/homepage/homepage.component';

const routes: Routes = [
  {
    path:'',component:HomepageComponent
  },
  {
    path:'vegfoods',component:VegComponent
  },
  {
    path:'nonvegfoods',component:NonvegComponent
  },
  {
    path:'drinks',component:DrinksComponent
  },
  {
    path:'deserts',component:DesertComponent
  },
  {
    path:'starters',component:StarterComponent
  },
  {
    path:'purchase',component:FormsComponent
  },
  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
