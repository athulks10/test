import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VegComponent } from './veg/veg.component';
import { NonvegComponent } from './nonveg/nonveg.component';
import { DrinksComponent } from './drinks/drinks.component';
import { RouterModule } from '@angular/router';
import { DesertComponent } from './desert/desert.component';
import { StarterComponent } from './starter/starter.component';
import { HomepageModule } from 'src/app/homepage/homepage.module';
import { FormsComponent } from './forms/forms.component';
import { FormsModule } from '@angular/forms';
import { BottomComponent } from './bottom/bottom.component';



@NgModule({
  declarations: [
    VegComponent,
    NonvegComponent,
    DrinksComponent,
    DesertComponent,
    StarterComponent,
    FormsComponent,
    BottomComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    HomepageModule,
    FormsModule
  ],
exports:[
  VegComponent,
  NonvegComponent,
  DrinksComponent,
  DesertComponent,
  StarterComponent,
  FormsComponent
]
})
export class RestuarentsModule { }
