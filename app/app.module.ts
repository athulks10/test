import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RestuarentsModule } from 'src/restuarents/restuarents.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomepageModule } from './homepage/homepage.module';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomepageModule,
    RestuarentsModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
